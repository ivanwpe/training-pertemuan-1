/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan.training_pertemuan_1;

import java.util.*;
/**
 *
 * @author ivan
 */
public class PertemuanSatu {
    //pake static biar bisa nyambung sama yang dibawah
    final static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args){
//        System.out.print("Masukan score : ");
//        int score = input.nextInt();

//        for(int a=0 ; a<10 ; a++){
//            System.out.println(a + " ");
//        }
        
//        int a = 0;
//        do{
//            a++;
//            System.out.println(a + " ");
//        }while(a < 10);
//        
//        String[] listNama = {"ASD","QWE","ZXC"};
//        
//        for(var list:listNama){
//            System.out.println(list);
//        }
        
//        switch(score) {
//            case 1:
//                System.out.println("score 1");
//                break;
//            case 2:
//                System.out.println("score 2");
//                break;
//            default:
//                 System.out.println("out of menu");
//        }

//        if(score < 80){
//            System.out.println("Kurang");
//        }
//        else if(score < 70){
//            System.out.println("Kurang Banget");
//        }
//        else{
//            System.out.println("Parah");
//        }


// 1.
//        int i = 0;
//        int num = 0;
//        
//        String primeNumber = "";
//        System.out.println("Input : ");
//        int n = input.nextInt();
//        
//        for(i=1 ; i<=n ; i++){
//            int count = 0;
//            for(num=i ; num>=1 ; num--){
//                if(i % num == 0){
//                    count = count + 1;
//                }
//            }
//            if(count == 2){
//                primeNumber = primeNumber + i + " ";
//            }
//        }
//        System.out.println(primeNumber);


// 4.
//        int bebek = 0;
//        int kandang = 0;
//        int hasil = 0;
//        int output;
//        
//        System.out.println("Jumlah bebek : ");
//        bebek = input.nextInt();
//        System.out.println("Jumlah bebek maksimal masuk kandang : ");
//        kandang = input.nextInt();
//        
//        hasil = bebek/kandang;
//        
//        if(bebek % kandang > 0){
//            output = hasil + 1;
//            System.out.println("Jumlah kandang" + output);
//        }else 
//            System.out.println("Jumlah kandang" + hasil);


// 5.
//        System.out.println("input : ");
//        int masuk = input.nextInt();
//        
//        if(masuk % 2 == 0){
//            System.out.print("a");
//        }
//        if(masuk % 3 == 0){
//            System.out.print("b");
//        }
//        if(masuk % 5 == 0){
//            System.out.print("c");
//        }
//        if(masuk % 7 == 0){
//            System.out.print("d");
//        }
//        if(masuk % 10 == 0){
//            System.out.print("e");
//        }
//        if(masuk % 15 == 0){
//            System.out.print("f");
//        }  


// 2.
//        int pow = 1;
//        int output = 0;
//        
//        for(int i=0 ; i<7 ; i++){
//            output = output + pow*pow;
//            System.out.print(output + " ");
//            pow++;
//        }


// 3.
//        int output = 0;
//        int pow = 2;
//        
//        for(int i=0 ; i<7 ; i++){
//            output = pow*pow*pow;
//            System.out.print(output + " ");
//            pow++;           
//        }


// 6.        

        System.out.print("start : ");
        int start = input.nextInt();
        System.out.print("end : ");
        int end = input.nextInt();
        
        System.out.print("bilangan ganjil antara " + start + 
                        " dan " + end + " adalah ");
        
        for(int i = start + 1 ; i < end ; i++){
            if(i % 2 != 0){
                System.out.print(i + " ");
            }           
        }
        
        System.out.println("");
        
        System.out.print("bilangan genap antara " + start + 
                        " dan " + end + " adalah ");
        
        for(int j = start + 1 ; j < end ; j++){
            if(j % 2 != 1){
                System.out.print(j + " ");
            }
        }
    }
}
